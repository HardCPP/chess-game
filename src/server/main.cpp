#include <QCoreApplication>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>

#include "config.h"
#include "session.h"

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    QTcpServer server;
    Config &config = Config::instance();

    QObject::connect(&server, &QTcpServer::newConnection, &server, [&]() {
        QTcpSocket *clientSocket = server.nextPendingConnection();

        Session *session = new Session(clientSocket);
        QObject::connect(session, &Session::sessionEnded, session, &Session::deleteLater);

        session->onConnect();
    });

    if (!server.listen(QHostAddress(config.host()), config.port())) {
        qFatal("Impossible de démarrer le serveur");
    }

    return app.exec();
}
