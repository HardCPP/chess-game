#include "session.h"

Session::Session(QTcpSocket *clientSocket, QObject *parent) : QObject(parent), clientSocket(clientSocket) {
    connect(clientSocket, &QTcpSocket::readyRead, this, &Session::onReadyRead);
    connect(clientSocket, &QTcpSocket::disconnected, this, &Session::onDisconnected);
}

void Session::onReadyRead() {
    QByteArray data = clientSocket->readAll();
    handleMessage(data);
}

void Session::onDisconnected() {
    qDebug() << "Le client s'est déconnecté";
    emit sessionEnded();
    delete this;
}

void Session::onConnect() {
    qDebug() << "Nouvelle connexion client";
}

void Session::handleMessage(const QByteArray &data) {
    Q_UNUSED(data);
    QByteArray response;
    sendResponse(response);
}

void Session::sendResponse(const QByteArray &response) {
    clientSocket->write(response);
}
