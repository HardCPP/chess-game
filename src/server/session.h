#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include <QTcpSocket>

class Session : public QObject {
    Q_OBJECT

public:
    explicit Session(QTcpSocket *clientSocket, QObject *parent = nullptr);

signals:
    void sessionEnded();

public slots:
    void onReadyRead();
    void onDisconnected();
    void onConnect();

private:
    QTcpSocket *clientSocket;


    void handleMessage(const QByteArray &data);
    void sendResponse(const QByteArray &response);
};

#endif // SESSION_H
