#include <QDir>

#include "config.h"

Config::Config() : settings(QDir::currentPath() + "/../common/config.ini", QSettings::IniFormat) {}

Config &Config::instance() {
    static Config instance;
    return instance;
}

QString Config::host() const {
    return settings.value("server/host", "127.0.0.1").toString();
}

int Config::port() const {
    return settings.value("server/port", 1234).toInt();
}
