#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QSettings>

class Config {
public:
    static Config &instance();

    QString host() const;
    int port() const;

private:
    QSettings settings;

    Config();
    ~Config() = default;

    Config(const Config &) = delete;
    Config &operator=(const Config &) = delete;
};

#endif //CONFIG_H
