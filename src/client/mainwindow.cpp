#include <QMessageBox>
#include <QSplashScreen>
#include <QTimer>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(Network *network, Config &config, QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow), network(network) {
    ui->setupUi(this);

    connect(network, &Network::connected, this, [=]() {
        qDebug("Connecté au serveur");
    });

    connect(network, &Network::disconnected, this, [=]() {
        qDebug("Déconnecté du serveur");
    });

    connect(network, &Network::dataReceived, this, [=](const QByteArray &data) {
        qDebug("Données reçues : %s", data.constData());
    });

    connect(network, &Network::errorOccurred, this, [=](QAbstractSocket::SocketError socketError) {
        qDebug("Erreur de connexion : %d", socketError);
    });

    connect(network, &Network::connectionFailed, this, &MainWindow::showConnectionError);

    network->connectToServer(config.host(), config.port());
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    network->disconnectFromServer();
    QMainWindow::closeEvent(event);
}

void MainWindow::showConnectionError(const QString &errorMessage) {
    connectionError = errorMessage;
}

void MainWindow::displayConnectionError() {
    if (!connectionError.isEmpty()) {
        QMessageBox::critical(this, "Erreur de connexion", connectionError);
    }
}

void MainWindow::start() {
    loadingDialog = new LoadingDialog(this);
    loadingDialog->show();
    loadingDialog->startLoading();

    QObject::connect(loadingDialog, &LoadingDialog::loadingFinished, this, &MainWindow::onLoadingFinished);
    QObject::connect(network, &Network::connectionFailed, this, &MainWindow::showConnectionError);
}

void MainWindow::onLoadingFinished() {
    loadingDialog->close();
    delete loadingDialog;

    QPixmap pixmap(":/images/splash.png");
    QSplashScreen splash(pixmap);
    splash.show();

    displayConnectionError();

    splash.finish(this);

    show();
}
