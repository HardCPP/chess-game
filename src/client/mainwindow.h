#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>

#include "config.h"
#include "network.h"
#include "loadingdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(Network *network, Config &config, QWidget *parent = nullptr);
    ~MainWindow() override;
    void start();

protected:
    void closeEvent(QCloseEvent *event) override;

public slots:
    void displayConnectionError();

private slots:
    void showConnectionError(const QString &errorMessage);
    void onLoadingFinished();

private:
    Ui::MainWindow *ui;
    Network *network;
    LoadingDialog *loadingDialog;
    QString connectionError;
};

#endif // MAINWINDOW_H
