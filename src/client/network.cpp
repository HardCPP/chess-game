#include "network.h"
#include <QHostAddress>

Network::Network(QObject *parent) : QObject(parent) {
    socket = new QTcpSocket(this);

    connect(socket, &QTcpSocket::connected, this, &Network::onConnected);
    connect(socket, &QTcpSocket::disconnected, this, &Network::onDisconnected);
    connect(socket, &QTcpSocket::readyRead, this, &Network::onReadyRead);
    connect(socket, &QTcpSocket::errorOccurred, this, &Network::onError);
}

void Network::connectToServer(const QString &host, int port) {
    socket->connectToHost(QHostAddress(host), port);
}

void Network::onConnected() {
    emit connected();
}

void Network::onDisconnected() {
    emit disconnected();
}

void Network::onReadyRead() {
    QByteArray data = socket->readAll();
    emit dataReceived(data);
}

void Network::onError(QAbstractSocket::SocketError socketError) {
    emit errorOccurred(socketError);
    if (socketError == QAbstractSocket::ConnectionRefusedError ||
        socketError == QAbstractSocket::HostNotFoundError ||
        socketError == QAbstractSocket::SocketTimeoutError) {
        emit connectionFailed(tr("Erreur de connexion: %1").arg(socket->errorString()));
    }
}

void Network::disconnectFromServer() {
    if (socket->state() == QAbstractSocket::ConnectedState) {
        socket->disconnectFromHost();
    }
}
