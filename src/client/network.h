#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QTcpSocket>

class Network : public QObject {
    Q_OBJECT

public:
    explicit Network(QObject *parent = nullptr);
    void connectToServer(const QString &host, int port);
    void disconnectFromServer();

signals:
    void connected();
    void disconnected();
    void dataReceived(const QByteArray &data);
    void errorOccurred(QAbstractSocket::SocketError socketError);
    void connectionFailed(const QString &errorMessage);

public slots:
    void onConnected();
    void onDisconnected();
    void onReadyRead();
    void onError(QAbstractSocket::SocketError socketError);

private:
    QTcpSocket *socket;
};

#endif // NETWORK_H
