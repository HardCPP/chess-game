#include <QApplication>

#include "mainwindow.h"
#include "config.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    Config &config = Config::instance();

    Network network;
    MainWindow mainWindow(&network, config);
    mainWindow.start();

    return app.exec();
}
