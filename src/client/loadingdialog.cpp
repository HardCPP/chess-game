#include <QTimer>

#include "loadingdialog.h"
#include "ui_loadingdialog.h"

LoadingDialog::LoadingDialog(QWidget *parent) : QDialog(parent), ui(new Ui::LoadingDialog) {
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
}

LoadingDialog::~LoadingDialog() {
    delete ui;
}

void LoadingDialog::startLoading() {
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [&, timer]() {
        int value = ui->progressBar->value();
        if (value >= 100) {
            timer->stop();
            emit loadingFinished();
        } else {
            ui->progressBar->setValue(value +1);
        }
    });

    timer->start(35);
}

QProgressBar *LoadingDialog::getProgressBar() const {
    return ui->progressBar;
}
