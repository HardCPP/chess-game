#ifndef LOADINGDIALOG_H
#define LOADINGDIALOG_H

#include <QDialog>
#include <QProgressBar>

namespace Ui {
    class LoadingDialog;
}

class LoadingDialog : public QDialog {
    Q_OBJECT

public:
    explicit LoadingDialog(QWidget *parent = nullptr);
    ~LoadingDialog();

    void startLoading();

    QProgressBar *getProgressBar() const;

signals:
    void loadingFinished();

private:
    Ui::LoadingDialog *ui;
};

#endif // LOADINGDIALOG_H
